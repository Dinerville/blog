---
id: seleniumMethods
title: Selenium методы
---

## Selenium методы
`driver.CurrentWindowHandle` // айди текущей вкладки  
`driver.PageSource` // html страницы  
`driver.Title` // тайтл страницы  
`driver.Url` // урл страницы  
`driver.WindowHandles` // айдишки всех открытых таб  

`driver.Close()` // закрыть текущую вкладку  
`driver.Quit()` // закрыть браузер  

`driver.Manage().Cookies.AllCookies` // получить все куки  
`driver.Manage().Cookies.AddCookie()` // добавить куки  
`driver.Manage().Cookies.DeleteAllCookies()` // удалить все куки  
`driver.Manage().Cookies.DeleteCookie()` // удалить определенное куки  
`driver.Manage().Cookies.DeleteCookieNamed()` // удалить куки с определенным названием  
`driver.Manage().Cookies.GetCookieNamed()` // взять куки с определенным называнием  

`driver.Manage().Logs.AvailableLogTypes` // доступные типы логов
`driver.Manage().Logs.GetLog()` // получить все логи опрееленного типа  
`driver.Manage().Window.Position.IsEmpty` // возвращает пустые ли координаты точек, показывающих на положения окна на мониторе  
`driver.Manage().Window.Position.X` // возвращает отступ окна браузера в пикселях от левого края монитора   
`driver.Manage().Window.Position.Y` // возвращает отступ окна браузера в пикселях от верхнего края монитора  
`driver.Manage().Window.Position.Offset()` // сдвинуть окно браузера на другие координаты  

`driver.Manage().Window.Size.IsEmpty` // возвращает не пустые ли координаты ширины и длинны окна
`driver.Manage().Window.Size.Height` // высота окна в пикселях
`driver.Manage().Window.Size.Width` // ширина окна в пикселях

driver.Manage().Window.FullScreen() // переводит браузер в полноэкранный режим, то же что нажатие на F11
driver.Manage().Window.Maximize() // расширяет окно браузера на длинну монитора
driver.Manage().Window.Minimize() // ?

driver.Manage().Timeouts().AsynchronousJavaScript // сколько времени ждать выполнения джаваскрипта, который выполняет сам селениум когда дается такая команда
driver.Manage().Timeouts().ImplicitWait // неявное ожидание. Сколько ждать пока не появится элемент
driver.Manage().Timeouts().PageLoad // сколько ждать загрузки страницы

driver.SwitchTo().Window() // переключится на вкладку с определенной айдишкой
driver.SwitchTo().ActiveElement() // получить элемент у которого фокус в данный момент

driver.SwitchTo().Alert().Text // получить текст алерта
driver.SwitchTo().Alert().Accept() // нажать принять на алерте
driver.SwitchTo().Alert().Dismiss() // нажать заблокировать на алерте
driver.SwitchTo().Alert().SendKeys() // напечатать в алерте текст
driver.SwitchTo().Alert().SetAuthenticationCredentials() // вписать логин и пароль в алерт который требует авторизации

driver.SwitchTo().DefaultContent() // переключится в самый главный фрейм, то есть выйти из всех фреймов
driver.SwitchTo().Frame() // переключится на фрейм на странице
driver.SwitchTo().ParentFrame() // переключится на фрейм на уровень выше чем текущий

driver.Navigate().Back() // нажать кнопку назад в браузере
driver.Navigate().Forward() //нажать кнопку вперед в браузере
driver.Navigate().GoToUrl() // перейти по определенному урлу в браузере
driver.Navigate().Refresh() // перезагрузить вкладку

driver.FindElement() // найти элемент на странице
driver.FindElements() // найти элементы на странице

driver.ExecuteJavaScript() //  выполнить джаваскрипт на странице
driver.TakeScreenshot() // сделать скриншот видимой области браузера

## Работа с элементом
driver.FindElement(el).Size.IsEmpty // возвращает есть ли значение ширины и высоты этого элемента
driver.FindElement(el).Size.Height // высота элемента в пикселях
driver.FindElement(el).Size.Width // ширина элемента в пикселях
driver.FindElement(el).Text // возвращает текст в элементе
driver.FindElement(el).Displayed // отображается ли элемент на экране или нет
driver.FindElement(el).Enabled // для всего возвращает тру, кроме тех элементов где есть тег disabled, Например < button disabled / > вернет false

 


